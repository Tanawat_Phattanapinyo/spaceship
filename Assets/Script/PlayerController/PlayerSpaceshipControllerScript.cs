﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spaceship;
using Unity.Mathematics;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerController
{
    public class PlayerSpaceshipControllerScript : MonoBehaviour
    {
        [SerializeField] private float gravityForce;
        [SerializeField] private float distanceThreshold;
        [SerializeField] private Transform planet;
        [SerializeField] private PlayerSpaceship playerSpaceship;

        private Vector2 spaceShipMovementInput = Vector2.zero;
        private SpriteRenderer spriteRenderer;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
        private float cooldownTime = 2;
        private float nextFire;
        
        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            MovementBoundary();
        }

        // Start is called before the first frame update
        void Start()
        {
            Application.targetFrameRate = -1;
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            OnFire();
        }

        private void Move()
        {
            // Move with Gravity
            var directionToPlanet = planet.position - transform.position;
            var gravity = directionToPlanet.magnitude <= distanceThreshold ? directionToPlanet.normalized * gravityForce : Vector3.zero;
            var inputVelocity = spaceShipMovementInput * playerSpaceship.Speed;
            var moveVelocity = inputVelocity + (Vector2)gravity;
            
            // Movement
            var newPosition = transform.position;
            newPosition.x = transform.position.x + moveVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + moveVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);
            transform.position = newPosition;
        }
        
        private void MovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "mainCamera can't be null!");

            var offsetDistance = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offsetDistance.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offsetDistance.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offsetDistance.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offsetDistance.y / 2;
        }

        private void OnFire()
        {
            if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFire)
            {
                nextFire = Time.time + cooldownTime;
                playerSpaceship.Fire();
            }
        }
        
        public void OnMovement(InputAction.CallbackContext context)
        {
            spaceShipMovementInput = context.ReadValue<Vector2>();
        }
    }
}

