﻿using Script.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Manager
{
    public class LevelManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private Text levelText;
        public static int levelCount = 1;

        private void Start()
        {
            levelText = GetComponent<Text>();
        }

        private void Update()
        {
           levelText.text = "Level: " + levelCount;
        }
    }
}