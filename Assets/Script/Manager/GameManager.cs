﻿using Script.Utilities;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Script.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship bossSpaceship;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private GameObject planet;
        [SerializeField] private RectTransform dialogInGame;
        [SerializeField] private RectTransform dialogHighScore;
        [SerializeField] private RectTransform dialogButton;
        [SerializeField] private RectTransform dialogEndGame;
        [SerializeField] private RectTransform dialogNextLevel;
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;

        private PlayerSpaceship player;
        private EnemySpaceship enemy;
        private EnemySpaceship boss;

        private void Awake()
        {
            MainMenu();
        }

        public void MainMenu()
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            ScoreManager.ScoreCount = 0;
            LevelManager.levelCount = 1;
            restartButton.gameObject.SetActive(false);
            dialogInGame.gameObject.SetActive(false);
            dialogEndGame.gameObject.SetActive(false);
            dialogNextLevel.gameObject.SetActive(false);
            dialogButton.gameObject.SetActive(true);
            dialogHighScore.gameObject.SetActive(true);
            startButton.onClick.AddListener(OnStartButtonClicked);
            quitButton.onClick.AddListener(OnQuitButtonClicked);
        }
        
        public void RestartMenu()
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            restartButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
            dialogButton.gameObject.SetActive(true);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
        }
        
        private void StartGame()
        {
            FirstState();
        }
        
        private void RestartGame()
        {
            RestartMenu();
            audioSource.Stop();
        }
        
        private void SpawnPlayerSpaceship(int hp,int speed)
        {
            player = Instantiate(playerSpaceship);
            player.CrateSpaceship(hp,speed);
            player.OnExploded += RestartGame;
        }

        private void SpawnEnemySpaceship(int hp,int speed)
        {
            enemy = Instantiate(enemySpaceship);
            enemy.CrateEnemySpaceship(hp,speed);
        }
        
        private void SpawnBossSpaceship(int hp,int speed)
        {
            boss = Instantiate(bossSpaceship);
            boss.CrateEnemySpaceship(hp,speed);
        }

        private void FirstState()
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.BGMFirstState);
            SpawnPlayerSpaceship(100,3);
            SpawnEnemySpaceship(100,5);
            Instantiate(planet);
            enemy.OnExploded += NextLevel;
        }

        public void SecondState()
        {
            Cursor.lockState = CursorLockMode.Locked;
            dialogNextLevel.gameObject.SetActive(false);
            dialogInGame.gameObject.SetActive(true);
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.BGMSecondState);
            LevelManager.levelCount += 1;
            SpawnBossSpaceship(200,10);
            boss.OnExploded += EndGame;
        }

        private void NextLevel()
        {
            audioSource.Stop();
            Cursor.lockState = CursorLockMode.None;
            dialogNextLevel.gameObject.SetActive(true);
            dialogInGame.gameObject.SetActive(false);
        }

        public void EndGame()
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            dialogEndGame.gameObject.SetActive(true);
            dialogInGame.gameObject.SetActive(false);
            audioSource.Stop();
        }

        public void OnMenuButtonClicked()
        {
            SceneManager.LoadScene(0);
        }
        
        private void OnStartButtonClicked()
        {
            dialogInGame.gameObject.SetActive(true);
            dialogHighScore.gameObject.SetActive(false);
            dialogButton.gameObject.SetActive(false);
            StartGame();
        }

        private void OnRestartButtonClicked()
        {
            restartButton.gameObject.SetActive(false);
            dialogButton.gameObject.SetActive(false);
            dialogHighScore.gameObject.SetActive(false);
            dialogInGame.gameObject.SetActive(false);
            ScoreManager.ScoreCount = 0;
            SceneManager.LoadScene(0);
        }
        
        public void OnQuitButtonClicked()
        {
            Debug.Log("Quit the game!");
            Application.Quit();
        }
    }
}