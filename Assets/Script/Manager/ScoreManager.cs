﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Script.Utilities;

namespace Script.Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private Text scoreText;
        [SerializeField] private Text highScoreText;
        [SerializeField] private Text currentScoreText;
        [SerializeField] private Text showScore;
        
        public static int ScoreCount;
        private static int highScore;
        private static int currentScore;

        private void Start()
        {
            scoreText.GetComponent<Text>();
            highScoreText.GetComponent<Text>();
            currentScoreText.GetComponent<Text>();
            showScore.GetComponent<Text>();
        }

        private void Update()
        {
            currentScore = ScoreCount;
            
            scoreText.text = "Score: " + ScoreCount;
            highScoreText.text = "High Score: " + highScore;
            currentScoreText.text = "Your Score: " + currentScore;
            showScore.text = "Your Score: " + currentScore;

            if (ScoreCount > highScore)
            {
                highScore = ScoreCount;
            }
        }
    }
}