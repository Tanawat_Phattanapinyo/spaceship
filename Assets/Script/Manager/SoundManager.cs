﻿using System;
using System.Security;
using Script.Utilities;
using UnityEngine;

namespace Script.Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        public static SoundManager Instance { get; private set; }

        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }
        
        public enum Sound
        {
            BGMFirstState,
            BGMSecondState,
            EnemyFire,
            EnemyDead,
            PlayerFire,
            PlayerDead,
        }

        /// <summary>
        /// Play sound
        /// </summary>
        /// <param name="audioSource"></param>
        /// <param name="sound"></param>
        public void Play(AudioSource audioSource, Sound sound)
        {
            Debug.Assert(audioSource != null,"audioSource can't be null");

            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }
            Debug.Assert(false,$"Cannot find sound {sound}");
            return null;
        }

        private void Awake()
        {
            Debug.Assert(audioSource != null,"audioSource can't be null");
            Debug.Assert(soundClips != null && soundClips.Length != 0,"soundClips can't be null && soundClips.Length != 0");

            if (Instance == null)
            {
                Instance = this;
            }
        }
    }
}