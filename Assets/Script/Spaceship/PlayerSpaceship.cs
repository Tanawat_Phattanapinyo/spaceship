﻿using System;
using UnityEngine;
using PlayerController;
using Script.Manager;
using Unity.Mathematics;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        [SerializeField] private Transform spaceshipPosition;
        [SerializeField] private Transform gunPosition;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private GameObject explosion;
        [SerializeField] private Renderer colorTakeHit;
        
        private float cooldownTime = 1;
        private float defaultColor;
        
        public event Action OnExploded;

        private void Update()
        {
            hpText.text = $"Player HP: {HP}/{MaxHP}";
            if(HP <= 0)
            {
                HP = 0;
            }
            if (Time.time > defaultColor)
            {
                defaultColor = Time.time + cooldownTime;
                colorTakeHit.material.color = Color.white;
            }
        }
        
        public void TakeHit(int damage)
        {
            HP -= damage;
            colorTakeHit.material.color = Color.red;
            
            if (HP > 0)
            {
                return;
            }
            colorTakeHit.material.color = Color.clear;
            Explode();
        }
        
        public void CrateSpaceship(int maxHp, float speed)
        {
            base.CreateSpaceship(maxHp, speed);
        }

        public void Explode()
        {
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerDead);
            GameObject explosionEffect = Instantiate(explosion, spaceshipPosition.position, quaternion.identity);
            Destroy(explosionEffect.gameObject,2f);
            Destroy(gameObject,0.6f);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            Debug.Log("Shooting!");
            Instantiate(bulletPrefab,gunPosition.position,quaternion.identity);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
        }
    }
}
