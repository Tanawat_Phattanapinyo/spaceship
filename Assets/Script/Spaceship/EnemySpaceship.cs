﻿using System;
using Script.Manager;
using Unity.Mathematics;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        [SerializeField] private Transform enemySpaceshipPosition;
        [SerializeField] private Transform gunPosition;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private GameObject explosion;
        [SerializeField] private Renderer colorTakeHit;
        
        private float cooldownTime = 1;
        private float defaultColor;
        
        public event Action OnExploded;

        private void Update()
        {
            hpText.text = $"Enemy HP: {HP}/{MaxHP}";
            if (Time.time > defaultColor)
            {
                defaultColor = Time.time + cooldownTime;
                colorTakeHit.material.color = Color.white;
            }
        }

        public void CrateEnemySpaceship(int maxHp, float speed)
        {
            base.CreateSpaceship(maxHp, speed);
        }
        public void TakeHit(int damage)
        {
           HP -= damage;
           colorTakeHit.material.color = Color.red;

            if (HP > 0)
            {
                return;
            }
            colorTakeHit.material.color = Color.clear;
            Explode();
        }
        public void Explode()
        {
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyDead);
            GameObject explosionEffect = Instantiate(explosion, enemySpaceshipPosition.position, quaternion.identity);
            Destroy(explosionEffect.gameObject,2f);
            Destroy(gameObject,0.6f);
            OnExploded?.Invoke();
            ScoreManager.ScoreCount += 1;
        }

        public override void Fire()
        {
            Instantiate(bulletPrefab,gunPosition.position,quaternion.identity);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyFire);
        }
    }
}