﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        public AudioSource audioSource;
        public Text hpText;
        public int HP { get; protected set; }
        public int MaxHP { get; protected set; }
        public float Speed { get; private set; }
        
        public void Awake()
        {
            Application.targetFrameRate = -1;
        }
        
        public void Start()
        {
            hpText.GetComponent<Text>();
        }

        protected void CreateSpaceship(int maxHp, float speed)
        {
            HP = maxHp;
            MaxHP = maxHp;
            Speed = speed;
        }

        public abstract void Fire();
    }
}