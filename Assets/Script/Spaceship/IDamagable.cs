﻿namespace Spaceship
{
    public interface IDamagable
    {
        void TakeHit(int damage);
        void Explode();
    }
}