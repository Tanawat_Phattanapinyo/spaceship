﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rbd;
        
        private Vector2 screenBounds;

        // Start is called before the first frame update
        void Start()
        {
            rbd = GetComponent<Rigidbody2D>();
            rbd.velocity = new Vector2(0, speed);
            screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        }

        // Update is called once per frame
        void Update()
        {
            if (transform.position.y > screenBounds.y || transform.position.y < screenBounds.y * -1)
            {
                Destroy(gameObject);
            }
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target.TakeHit(damage);
            Destroy(gameObject);
        }
    }
}
