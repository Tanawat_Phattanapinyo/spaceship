﻿using System;
using System.Collections;
using System.Collections.Generic;
using Script.Manager;
using Spaceship;
using UnityEngine;

namespace EnemyController
{
        public class EnemySpaceshipControllerScript : MonoBehaviour
        {
            [SerializeField] private EnemySpaceship enemySpaceship;
            [SerializeField] private Transform player;
            [SerializeField] private float minDistanceToPlayer;
            [SerializeField] private float rotationDegrees;
            [SerializeField] private float cooldownTime;

            private float shootTime;
            private float oneSecTime;

            private void Awake()
            {
                player = FindObjectOfType<PlayerSpaceship>().GetComponent<Transform>();
                Application.targetFrameRate = -1;
            }

            // Update is called once per frame
            void Update()
            {
                Move();
                OnFire();
            }

            public void Move()
            {
                if (player == null)
                {
                    return;
                }

                //Distance To Player
                Vector2 displacementFromPlayer = (player.transform.position - transform.position);
                Vector2 directionToPlayer = displacementFromPlayer.normalized;
                Vector2 enemyVelocity = directionToPlayer * enemySpaceship.Speed;
             
                //Rotation
                var newXPosition = (player.transform.position.x - transform.position.x) * Time.deltaTime * enemySpaceship.Speed;
                var newYPosition = (player.transform.position.y - transform.position.y) * Time.deltaTime * enemySpaceship.Speed;
                var angleSpaceShip = Mathf.Atan2(newYPosition, newXPosition) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(new Vector3(0,0, angleSpaceShip + rotationDegrees));
                
                if (displacementFromPlayer.magnitude > minDistanceToPlayer)
                {
                     transform.Translate(enemyVelocity * Time.deltaTime);
                }
            }

            public void OnFire()
            {
                oneSecTime += Time.deltaTime;
                if (oneSecTime < 1)
                    return;
                {
                    shootTime--;
                    oneSecTime = 0;
                    if (shootTime <= 0)
                    {
                        enemySpaceship.Fire();
                        shootTime = cooldownTime;
                    }
                }
            }
        }
}

