﻿using System;
using UnityEngine;

namespace Script.Utilities
{
    // This code originate form http://wiki.unity3d.com/index.php/Singleton
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        // Check to see if we're about to be destroyed.
        private static bool isShuttingDown = false;
        private static object lockObject = new object();
        private static T instance;
 
        /// <summary>
        /// Access singleton instance through this propriety.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (isShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed. Returning null.");
                    return null;
                }
 
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        // Search for existing instance.
                        instance = (T)FindObjectOfType(typeof(T));
 
                        // Create new instance if one doesn't already exist.
                        if (instance == null)
                        {
                            // Need to create a new GameObject to attach the singleton to.
                            var singleton = new GameObject();
                            instance = singleton.AddComponent<T>();
                            singleton.name = instance.GetType().Name;
                        }
                    }
                    return instance;
                }
            }
        }

        protected void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(instance.gameObject);
            }
        }

        private void OnApplicationQuit()
        {
            isShuttingDown = true;
        }

        private void OnDestroy()
        {
            isShuttingDown = true;
        }
    }
}